<?php

namespace App\Http\Controllers;

use App\Berita;
use Illuminate\Http\Request;
use Image;
use Auth;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beritas = Berita::latest()->get();

        return view('berita.index', compact('beritas')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = Category::All();

        return view('berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'judul' => 'required',
            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($request->hasfile('filename'))
        {
           foreach($request->file('filename') as $image)
           {
               $name=$image->getClientOriginalName();
               $ext = $image->getClientOriginalExtension();
               $name=rand(100000,1001238912).".".$ext;
               $destinationPath = public_path('/upload/berita');
               $img = Image::make($image->getRealPath());
               $img->resize(800, 500, function ($constraint) {
                   $constraint->aspectRatio();
               })->save($destinationPath.'/'.$name);
               // $image->move(public_path().'/product/', 'std'.'_'.$nama);
               $data[] = $name;
               $user = Auth::user()->name;
            //    dd($img);
               Berita::create([
                'judul' => request('judul'),
                'isi' => request('isi'),
                'jenis' => request('jenis'),
                'gambar' => $name,
                'user' => $user
            ]);
               return redirect()->route('berita.index')->withSuccess('berita berhasil ditambahkan');
           }
        }
        echo "Tidak Ada Gambar";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show(Berita $berita)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|min:10'
        ]);
        Berita::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'jabatan' => request('jabatan')
        ]);
        return redirect()->route('berita.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit(Berita $berita)
    {
        $idbarang = $berita->id;
        $images = $berita->gambar;

        return view('berita.edit', compact('berita'));          /* $post dikirim view edit,dll*/
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Berita $berita)
    {

            if($request->hasfile('filename'))
            {
                // dd('ada gambar');
                $this->validate($request, [
                    'filename' => 'required',
                    'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                ]);

               foreach($request->file('filename') as $image)
               {
                $nama=$berita->gambar;
                unlink(public_path().'/upload/berita/'.$nama);
                $update = $request->All();
                $name=$image->getClientOriginalName();
                $ext = $image->getClientOriginalExtension();
                $name=rand(100000,1001238912).".".$ext;
                $destinationPath = public_path('/upload/berita');
                $img = Image::make($image->getRealPath());
                $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
                   })->save($destinationPath.'/'.$name);
                   // $image->move(public_path().'/product/', 'std'.'_'.$nama);
                $data[] = $name;
                $user = Auth::user()->name;
                //    dd($berita->gambar);
                $berita->gambar = $name;
                $berita->update($update);
               }
            }
            else{
            // dd('Tidak ada gambar');
            $update = $request->All();
            $update['gambar'] = $berita->gambar;
            $berita->update($update);
        }
        return redirect()->route('berita.index')->withSuccess('berita berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy(Berita $berita)
    {

        $nama=$berita->gambar;
        // dd($nama);
        unlink(public_path().'/upload/berita/'.$nama);
        $berita->delete();
        return redirect()->route('berita.index')->withDanger('data berhasil dihapus');
    }
}
