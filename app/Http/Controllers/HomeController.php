<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\User;
use App\Barang;
use App\Kategori;
use App\Images;
use App\Berita;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function dashboard()
    {
        $users = User::latest()->get();
        $valuser = $users->count();
        if (Auth::user()->jabatan == 'admin'){
            $barangs = Barang::latest()->get();
            $valbarang = $barangs->count();
        }
        elseif (Auth::user()->jabatan == 'siswa'){
            $barangs = Barang::where('penjual','LIKE',Auth::user()->name)->latest()->get();
            $valbarang = $barangs->count();
        }
        if (Auth::user()->jabatan == 'admin'){
            $carts = Cart::latest()->get();
            $valcart = $carts->count();
        }
        elseif (Auth::user()->jabatan == 'siswa'){
            $carts = Cart::where('penjual','LIKE',Auth::user()->name)->where('status','LIKE',"order")->latest()->get();
            $valcart = $carts->count();
        }
        elseif (Auth::user()->jabatan == 'pembeli'){
            $carts = Cart::where('user','LIKE',Auth::user()->name)->latest()->get();
            $valcart = $carts->count();
        }
        $kategoris = Kategori::latest()->get();
        $valkategori = $kategoris->count();
        $beritas = Berita::latest()->get();
        $valberita = $beritas->count();
        // dd($valcart);
        return view('dashboard', compact('valuser','valbarang','valkategori','valberita','valcart'));

    }
}
