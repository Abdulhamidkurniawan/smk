<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Images;
use App\Kategori;
use Illuminate\Http\Request;
Use Image;
use Auth;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->jabatan == 'admin'){$barangs = Barang::latest()->get();}
        if (Auth::user()->jabatan == 'siswa'){$barangs = Barang::where('penjual','LIKE',Auth::user()->name)->latest()->get();}

        return view('barang.index', compact('barangs')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::All();
        return view('barang.create', compact('kategoris'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $barang=Barang::create([
            'nama' => request('nama'),
            'kategori' => request('kategori'),
            'harga' => request('harga'),
            'stok' => request('stok'),
            'penjual' => request('penjual'),
            'deskripsi' => request('deskripsi'),
            'video' => request('video'),
            'promosi' => 'kosong',
            'terjual' => 0,
        ]);

    $this->validate($request, [
            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ]);
            // dd($request);
            // dd($barang,$request,$request->hasfile('filename'));

    if($request->hasfile('filename'))
     {
        foreach($request->file('filename') as $image)
        {
            $name=$image->getClientOriginalName();
            $ext = $image->getClientOriginalExtension();
            $nama=$barang->id.'_'.rand(100000,1001238912).".".$ext;
            $destinationPath = public_path('/product');
            $img = Image::make($image->getRealPath());
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$nama);
            // $image->move(public_path().'/product/', 'std'.'_'.$nama);
            $data[] = $nama;

            Images::create([
            'nama' => $nama,
            'idbarang' => $barang->id,
            ]);
            // dd($barang->id);

        }
     }
        return redirect()->route('barang.index')->withSuccess('data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        $idbarang = $barang->id;
        $kategoris = Kategori::All();
        $images = Images::where('idbarang', $idbarang)->get();
        // dd($idbarang,$images);
        return view('barang.edit', compact('barang','images','kategoris'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        $update = $request->All();
        $barang->update($update);
        // dd($update,$request->file('filename'),$request->hasfile('filename'));

        if($request->hasfile('filename'))
        {
            $this->validate($request, [
                'filename' => 'required',
                'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
           foreach($request->file('filename') as $image)
           {
               $name=$image->getClientOriginalName();
               $ext = $image->getClientOriginalExtension();
               $nama=$barang->id.'_'.rand(100000,1001238912).".".$ext;
               $destinationPath = public_path('/product');
               $img = Image::make($image->getRealPath());
               $img->resize(800, 800, function ($constraint) {
                   $constraint->aspectRatio();
               })->save($destinationPath.'/'.$nama);
               // $image->move(public_path().'/product/', 'std'.'_'.$nama);
               $data[] = $nama;

               Images::create([
               'nama' => $nama,
               'idbarang' => $barang->id,
               ]);

           }
        }
           return redirect()->route('barang.index')->withSuccess('data berhasil ditambahkan');
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        // dd($barang->id);
        $images = Images::where('idbarang','=',$barang->id)->get();
        foreach ($images as $image){
        // dd($image->nama);
        unlink(public_path().'/product/'.$image->nama);
        $image->delete();
        }
        $barang->delete();
        return redirect()->route('barang.index')->withDanger('data berhasil dihapus');
    }

    public function hapus($id)
    {
        // echo 'yang dikirim id='.$id;

        $todo = Images::findOrFail($id);
        // $nama=$todo->nama;

        unlink(public_path().'/product/'.$nama);
        $todo->delete();
        // return ('gambar berhasil dihapus');
    }
}
