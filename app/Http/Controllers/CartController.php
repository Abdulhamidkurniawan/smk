<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Barang;
use App\Kategori;
use App\Images;
use App\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nama= Auth::user()->name;
        if (Auth::user()->jabatan == 'admin'){$carts = Cart::latest()->get();}
        else if (Auth::user()->jabatan == 'siswa'){$carts = Cart::where('penjual','LIKE',$nama)->get();}
        else {$carts = Cart::where('user','=',$nama)->get();}

        // dd($nama,$carts);
        return view('cart.index', compact('carts')); /* kirim var */
    }

    public function cart()
    {
        $nama= Auth::user()->name;
        $carts = Cart::where('user','=',$nama)->where('status','=','cart')->get();

        // dd($nama,$carts);
        return view('home.cart', compact('carts')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $user=Auth::user()->name;
        $carts=Cart::create([
            'user' => $user,
            'idbarang' => request('idbarang'),
            'nama' => request('nama'),
            'kategori' => request('kategori'),
            'harga' => request('harga'),
            'jumlah' => request('quantity'),
            'penjual' => request('penjual'),
            'status' => 'cart',
        ]);
        // dd($carts);
        return back()->with('status', 'Barang ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        // dd($cart);
        $cart->delete();
        return redirect()->route('cart.index');
    }

    public function cartcheckout(Request $request, Cart $cart)
    {
        $nama= Auth::user()->name;
        $carts = Cart::where('user','=',$nama)->where('status','=','cart')->get();
        foreach ($carts as $order){
            $order->update([
                'status' => 'order',
            ]);
        }
        return redirect()->route('cart.index');
        // dd($carts);
    }

    public function end(Request $request, Cart $cart)
    {
        // dd($cart->jumlah);
        $cart->update([
            'status' => 'selesai'
        ]);
        $barangs = Barang::where('id','=',$cart->idbarang)->get();
        foreach ($barangs as $barang){
            if ($barang->stok >= $cart->jumlah){
            $stok = $barang->stok - $cart->jumlah;
            $terjual = $barang->terjual + $cart->jumlah;
            // dd($barang->nama,$barang->stok,$cart->jumlah,$stok,$terjual);
            $barang->update([
                'stok' => $stok,
                'terjual' => $terjual
            ]);
            }
            else {echo "Stok tidak mencukupi";}
        }

        return redirect()->route('cart.index');
        // dd($carts);
    }
}
