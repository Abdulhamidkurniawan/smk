<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Barang;
use App\Kategori;
use App\Images;
use App\Cart;
use App\Berita;
use EmbedServiceProvider;

class RedirectController extends Controller
{
    public function about(){return view('home.about');}
    public function blog(){
        $beritas = Berita::latest()->paginate(9);
        return view('home.blog', compact('beritas'));
    }
    public function blogdetail(Berita $berita){
        return view('home.blogdetail', compact('berita'));
    }
    public function checkout(){return view('home.checkout');}
    public function contact(){return view('home.contact');}
    public function detail(Barang $barang){
        $images=Images::where('idbarang','=',$barang->id)->get();
        // dd($barang->id,$images);
        return view('home.detail',compact('barang','images'));
    }
    public function index(){
        $bestsellers = Barang::orderBy('terjual', 'DESC')->limit(4)->get();
        $barus = Barang::latest()->limit(4)->get();
        $beritas = Berita::where('jenis','=','home')->latest()->limit(1)->get();

        return view('home.index', compact('bestsellers','barus','beritas'));
    }

    public function order(){return view('home.order');}
    public function shop(Request $request){
        $sort = $request->input('urutkan');
        $kategori = $request->input('kategori');
        if ($sort == "terbaru"){
            $barangs = Barang::latest()
            ->where('kategori','LIKE',$kategori)->paginate(9);
            $kategoris = Kategori::All();
        }
        elseif ($sort == "tertinggi"){
            $barangs = Barang::where('kategori','LIKE',$kategori)->orderBy('harga', 'DESC')->paginate(9);
            $kategoris = Kategori::All();

        }
        elseif ($sort == "terendah"){
            $barangs = Barang::where('kategori','LIKE',$kategori)->orderBy('harga', 'ASC')->paginate(9);
            $kategoris = Kategori::All();
        }
        else {
            $barangs = Barang::paginate(9);
            $kategoris = Kategori::All();
        }
        // $images = Images::where('idbarang', $idbarang)->get();
        // dd($kategoris);
        return view('home.shop', compact('barangs','kategoris','sort'));
    }

    public function kategori(Request $request){
        $sort = $request->input('urutkan');
        if ($sort == "terbaru"){
            $barangs = Barang::latest()->get();
            $kategoris = Kategori::All();
        }
        elseif ($sort == "tertinggi"){
            $barangs = Barang::orderBy('harga', 'DESC')->get();
            $kategoris = Kategori::All();

        }
        elseif ($sort == "terendah"){
            $barangs = Barang::orderBy('harga', 'ASC')->get();
            $kategoris = Kategori::All();
        }
        else {
            $barangs = Barang::all();
            $kategoris = Kategori::All();
        }
        // $images = Images::where('idbarang', $idbarang)->get();
        // dd($kategoris);
        return view('home.shop', compact('barangs','kategoris','sort'));
    }

    public function wishlist(){return view('home.wishlist');}
}
