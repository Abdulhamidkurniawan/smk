<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = ['nama','kategori','harga','stok','penjual','terjual','deskripsi','video','promosi','foto']; /* yang bsa di isi */
}
