<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'categories';
    protected $fillable = ['kategori','deskripsi','start','stop','foto','video']; /* yang bsa di isi */
}
