<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $fillable = ['judul','isi','jenis','gambar','user']; /* yang bsa di isi */
}
