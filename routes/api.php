<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/water', 'ApiWaterController@index');
Route::post('/water', 'ApiWaterController@create');
Route::get('/water/{water}', 'ApiWaterController@show');
Route::put('/water/{water}', 'ApiWaterController@update');
Route::delete('/water/{water}', 'ApiWaterController@destroy');
