<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

// Route::middleware('cors')->group(function() {
  Route::post('/loginapps', 'Auth\LoginController@login')->name('loginapps');
  // Route::post('/loginapps', function(){ return 'sukses'; });
// });

Route::get('/about', 'RedirectController@about')->name('about');
Route::get('/blog', 'RedirectController@blog')->name('blog');
Route::get('/blog/{berita}', 'RedirectController@blogdetail')->name('blogdetail');
Route::get('/checkout', 'RedirectController@checkout')->name('checkout');
Route::get('/contact', 'RedirectController@contact')->name('contact');
Route::get('/detail/{barang}', 'RedirectController@detail')->name('detail');
Route::get('/', 'RedirectController@index')->name('index');
Route::get('/order', 'RedirectController@order')->name('order');
Route::get('/shop', 'RedirectController@shop')->name('shop');

Route::get('/wishlist', 'RedirectController@wishlist')->name('wishlist');

Route::middleware('auth')->group(function() {

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart/create', 'CartController@store')->name('cart.store');
Route::delete('/cart/{cart}/delete', 'CartController@destroy')->name('cart.destroy');
Route::post('/cartcheckout', 'CartController@cartcheckout')->name('cart.checkout');
Route::get('/carthome', 'CartController@cart')->name('home.cart');
Route::get('/cart/{cart}/end', 'CartController@end')->name('cart.end');

// Route::middleware('admin')->group(function() {
    // Ruoute akun user
    Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
    Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
    Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
    // Route::get('/user_panel/{post}', 'UserPanelController@show')->name('user_panel.show');
    Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
    Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
    Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');

    Route::get('/barang', 'BarangController@index')->name('barang.index');
    Route::get('/barang/create', 'BarangController@create')->name('barang.create');
    Route::post('/barang/create', 'BarangController@store')->name('barang.store');
    Route::get('/barang/{barang}/edit', 'BarangController@edit')->name('barang.edit');
    Route::patch('/barang/{barang}/edit', 'BarangController@update')->name('barang.update');
    Route::delete('/barang/{barang}/delete', 'BarangController@destroy')->name('barang.destroy');
    Route::delete('/barang/deletefile/{id}', 'BarangController@hapus')->name('barang.hapus');

    Route::get('/kategori', 'KategoriController@index')->name('kategori.index');
    Route::get('/kategori/create', 'KategoriController@create')->name('kategori.create');
    Route::post('/kategori/create', 'KategoriController@store')->name('kategori.store');
    Route::get('/kategori/{kategori}/edit', 'KategoriController@edit')->name('kategori.edit');
    Route::patch('/kategori/{kategori}/edit', 'KategoriController@update')->name('kategori.update');
    Route::delete('/kategori/{kategori}/delete', 'KategoriController@destroy')->name('kategori.destroy');

    Route::get('/berita', 'BeritaController@index')->name('berita.index');
    Route::get('/berita/create', 'BeritaController@create')->name('berita.create');
    Route::post('/berita/create', 'BeritaController@store')->name('berita.store');
    Route::get('/berita/{berita}/edit', 'BeritaController@edit')->name('berita.edit');
    Route::patch('/berita/{berita}/edit', 'BeritaController@update')->name('berita.update');
    Route::delete('/berita/{berita}/delete', 'BeritaController@destroy')->name('berita.destroy');

    Route::get('/cart', 'CartController@index')->name('cart.index');
    Route::get('/cart/create', 'CartController@create')->name('cart.create');
    Route::post('/cart/create', 'CartController@store')->name('cart.store');
    // Route::get('/cart/{post}', 'CartController@show')->name('cart.show');
    Route::get('/cart/{user}/edit', 'CartController@edit')->name('cart.edit');
    Route::patch('/cart/{user}/edit', 'CartController@update')->name('cart.update');
    Route::delete('/cart/{user}/delete', 'CartController@destroy')->name('cart.destroy');

// });
});
