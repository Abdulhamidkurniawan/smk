<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name' => 'admin',
            'email' => 'admin@gmail.com',
            'jabatan' => 'admin',
            'password' => bcrypt('adadadad')],
            ['name' => 'siswa',
            'email' => 'siswa@gmail.com',
            'jabatan' => 'siswa',
            'password' => bcrypt('adadadad')],
            ['name' => 'pembeli',
            'email' => 'pembeli@gmail.com',
            'jabatan' => 'pembeli',
            'password' => bcrypt('adadadad')],
        ]);

        DB::table('jabatans')->insert([
            ['jabatan' => 'admin'],
            ['jabatan' => 'siswa'],
            ['jabatan' => 'pembeli'],
        ]);
    }
}
