<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('carts')) {
            //
        }
        else{
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->string('idbarang');
            $table->string('nama');
            $table->string('kategori');
            $table->string('harga');
            $table->integer('jumlah');
            $table->string('penjual');
            $table->string('status');
            $table->timestamps();
        });
    }
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
