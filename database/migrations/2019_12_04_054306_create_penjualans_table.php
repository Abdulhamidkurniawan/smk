<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenjualansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('penjualans')) {
            //
        }
        else{
            Schema::create('penjualans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('invoice');
                $table->string('nama');
                $table->integer('jumlah');
                $table->integer('harga');
                $table->string('pembeli');
                $table->string('penjual');
                $table->string('tanggal')->nullable();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualans');
    }
}
