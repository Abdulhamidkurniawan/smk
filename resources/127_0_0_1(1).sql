-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2020 at 03:41 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_pln`
--
CREATE DATABASE IF NOT EXISTS `inventory_pln` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `inventory_pln`;

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `koordinat` text COLLATE utf8mb4_unicode_ci,
  `tahun` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengadaan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `kode`, `nama`, `deskripsi`, `jenis`, `tipe`, `lokasi`, `ruang`, `koordinat`, `tahun`, `pengadaan`, `nomor`, `status`, `gambar`, `user`, `created_at`, `updated_at`) VALUES
(5, 'KRA.01.07.0002', 'Logitech g102', 'adadada', 'Elektronik', 'Mouse', 'KRA', 'Satpam', '-0.496548, 117.143926', '2019', 'PO', '0123', 'Baik', '40055168.jpg', 'wawan', '2019-05-05 19:58:45', '2019-06-27 11:00:29'),
(6, 'KRA.01.07.0003', 'Mouse', 'blsllslsls', 'Elektronik', 'Mouse', 'KRA', 'FO', '09128098213902', '2019', 'PO', '2', 'Baik', '165735988.PNG', 'wawan', '2019-07-21 17:47:33', '2019-07-21 17:47:33'),
(7, 'KRA.02.02.0004', 'Kursi Gaming', 'lslsl', 'Furniture', 'Kursi', 'KRA', 'FO', '09128098213902', '2019', 'PO', '222', 'Baik', '341967704.PNG', 'wawan', '2019-07-21 17:55:43', '2019-07-21 17:55:43'),
(8, 'KRA.01.07.0003', 'Mouse', NULL, 'Elektronik', 'Mouse', 'KRA', 'FO', NULL, '2019', 'PO', '2', 'Rusak', '165735988.PNG', 'wawan', '2019-07-22 02:25:03', '2019-07-22 02:25:03'),
(9, 'KRA.01.04.0004', 'Headset', NULL, 'Elektronik', 'CPU', 'KRA', 'FO', NULL, '2019', 'PO', '2', 'Baik', '690433086.jpg', 'wawan', '2019-07-22 03:42:36', '2019-07-22 03:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Elektronik', '01', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(2, 'Furniture', '02', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(3, 'Alat Kebersihan', '03', '2019-04-27 09:41:02', '2019-04-27 09:41:02'),
(4, 'Multimedia', '04', '2019-04-27 09:41:17', '2019-04-27 09:41:17'),
(5, 'Alat Makan', '05', '2019-04-27 09:41:30', '2019-04-27 09:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `kantors`
--

CREATE TABLE `kantors` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `singkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kantors`
--

INSERT INTO `kantors` (`id`, `nama`, `singkatan`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Kantor Mahakam', 'MHK', '01', '2019-04-27 08:26:07', '2019-04-27 08:26:07'),
(2, 'PLTD Karang Asam', 'KRA', '02', '2019-04-28 06:47:53', '2019-04-28 06:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_27_065333_create_barangs_table', 1),
(4, '2019_04_27_142412_create_kantors_table', 1),
(5, '2019_04_27_142608_create_jeniss_table', 1),
(6, '2019_04_27_142640_create_tipes_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengadaans`
--

CREATE TABLE `pengadaans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengadaans`
--

INSERT INTO `pengadaans` (`id`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(6, 'PO', 'PO', '2019-06-15 19:07:09', '2019-06-15 19:07:09'),
(8, 'Non PO', 'Non PO', '2019-06-15 19:08:23', '2019-06-15 19:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `tipes`
--

CREATE TABLE `tipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipes`
--

INSERT INTO `tipes` (`id`, `tipe`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Laptop', 'Elektronik', '01', '2019-04-27 09:06:20', '2019-04-27 09:21:04'),
(2, 'All in One PC', 'Elektronik', '02', '2019-04-29 16:00:00', '2019-04-29 16:00:00'),
(3, 'Monitor PC', 'Elektronik', '03', '2019-04-29 08:42:48', '2019-04-29 08:42:48'),
(4, 'CPU', 'Elektronik', '04', '2019-04-29 08:43:05', '2019-04-29 08:43:05'),
(5, 'Printer', 'Elektronik', '05', '2019-04-29 08:43:14', '2019-04-29 08:43:41'),
(6, 'Speaker', 'Elektronik', '06', '2019-04-29 08:43:34', '2019-04-29 08:43:47'),
(7, 'Mouse', 'Elektronik', '07', '2019-04-29 08:44:23', '2019-04-29 08:44:23'),
(8, 'LCD Projector', 'Elektronik', '08', '2019-04-29 08:44:40', '2019-04-29 08:44:40'),
(9, 'Televisi', 'Elektronik', '09', '2019-04-29 08:44:52', '2019-04-29 08:44:52'),
(10, 'Scanner', 'Elektronik', '10', '2019-04-29 08:45:09', '2019-04-29 08:45:09'),
(11, 'AC', 'Elektronik', '11', '2019-04-29 08:45:23', '2019-04-29 08:45:23'),
(12, 'Kulkas', 'Elektronik', '12', '2019-04-29 08:45:38', '2019-04-29 08:45:38'),
(13, 'DVR', 'Elektronik', '13', '2019-04-29 08:45:48', '2019-04-29 08:45:48'),
(14, 'Antena', 'Elektronik', '14', '2019-04-29 08:45:55', '2019-04-29 08:45:55'),
(15, 'Radio Accesspoint', 'Elektronik', '15', '2019-04-29 08:46:10', '2019-04-29 08:46:10'),
(16, 'Patrol Guard', 'Elektronik', '16', '2019-04-29 08:46:21', '2019-04-29 08:46:21'),
(17, 'CCTV', 'Elektronik', '17', '2019-04-29 08:46:30', '2019-04-29 08:46:30'),
(18, 'Meja', 'Furniture', '01', '2019-04-29 08:46:57', '2019-04-29 08:46:57'),
(19, 'Kursi', 'Furniture', '02', '2019-04-29 08:47:09', '2019-04-29 08:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', NULL, '$2y$10$/ZncElWocc7N8YuL6XKVwuicTYwzycMinIgTV76Yg2JKqEE6Uwayy', 'oBLGEPc5nAWnRzI1evlYnXj2VFP5i6ZTiBNa4C5V69ZghSWlf3yvqlO5Ayl5', '2019-04-27 08:25:51', '2019-04-30 22:33:35'),
(2, 'reza', 'reza@gmail.com', 'karyawan', NULL, '$2y$10$3rUYJ644aWgu.SPBYP5mjOtH4SK2ixS4sn8vrCRm0HcT00QT6CFvy', 'Ph3PsAH21pVSoromHeaVCfVKqyBUWLQhsEHQwqsZe4cYrTG2FCsMCBFBzT5O', '2019-05-01 07:20:13', '2019-06-24 16:10:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kantors`
--
ALTER TABLE `kantors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengadaans`
--
ALTER TABLE `pengadaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipes`
--
ALTER TABLE `tipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kantors`
--
ALTER TABLE `kantors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pengadaans`
--
ALTER TABLE `pengadaans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tipes`
--
ALTER TABLE `tipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Database: `laraveldropdown`
--
CREATE DATABASE IF NOT EXISTS `laraveldropdown` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `laraveldropdown`;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'US', NULL, NULL),
(2, 'UK', NULL, NULL),
(3, 'Australia', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Elektronik', '01', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(2, 'Furniture', '02', '2019-04-27 08:30:39', '2019-04-27 08:32:16'),
(3, 'Alat Kebersihan', '03', '2019-04-27 09:41:02', '2019-04-27 09:41:02'),
(4, 'Multimedia', '04', '2019-04-27 09:41:17', '2019-04-27 09:41:17'),
(5, 'Alat Makan', '05', '2019-04-27 09:41:30', '2019-04-27 09:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_28_180251_create_countries_table', 1),
(4, '2019_04_28_180324_create_states_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `countries_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `countries_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'NY', NULL, NULL),
(2, 3, 'Perth', NULL, NULL),
(3, 3, 'Sydney', NULL, NULL),
(4, 2, 'London', NULL, NULL),
(5, 1, 'Seattle', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipes`
--

CREATE TABLE `tipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipes`
--

INSERT INTO `tipes` (`id`, `tipe`, `jenis`, `kode`, `created_at`, `updated_at`) VALUES
(1, 'Laptop', 'Elektronik', '01', '2019-04-27 09:06:20', '2019-04-27 09:21:04'),
(2, 'Meja', 'Furniture', '01', '2019-04-27 09:39:45', '2019-04-27 09:40:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipes`
--
ALTER TABLE `tipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tipes`
--
ALTER TABLE `tipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `lora`
--
CREATE DATABASE IF NOT EXISTS `lora` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `lora`;

-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

CREATE TABLE `debits` (
  `id` int(10) UNSIGNED NOT NULL,
  `debit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(2, 'karyawan', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(3, 'mahasiswa', '2019-05-08 17:21:15', '2019-05-08 17:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_12_154812_create_debits_table', 2),
(5, '2019_11_12_162907_create_pumps_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pumps`
--

CREATE TABLE `pumps` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', NULL, '$2y$10$4BmZiu171qibVXe5IU5/B.qLzqYmgwPSYZmqbhep8o84TLwad2X5a', 'A0HHPPmhKbzTz2atErtEJ2IexWuy9kyuhp06ya3qApBGJHPm2fDVCOFIc0IJ', '2019-11-12 08:44:50', '2019-11-12 08:44:50'),
(3, 'coba', 'coba@gmail.com', 'karyawan', NULL, '$2y$10$CXgGg6UIy3hwWBQX573M0e.OH0m9ot/tqbjwtw9z.Jhb1lZAjim/i', NULL, '2019-11-13 10:10:33', '2019-11-13 10:10:33'),
(4, 'tes', 'tes@tes.com', 'admin', NULL, '$2y$10$.wTo.ytrf.WqUnGLsOgyxecEuq3V4WNFBKMaCzKILD3fn40qAuCeW', NULL, '2019-12-06 01:18:02', '2019-12-06 01:18:02'),
(5, 'dosen', 'dosen@dosen.com', 'admin', NULL, '$2y$10$WVSRPxham6lqssUT9SHjd.SAlTO599pYOceairuIe.vre8Hp0bkGO', NULL, '2019-12-06 01:19:07', '2019-12-06 01:19:07');

-- --------------------------------------------------------

--
-- Table structure for table `waters`
--

CREATE TABLE `waters` (
  `id` int(10) UNSIGNED NOT NULL,
  `usage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pumps`
--
ALTER TABLE `pumps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `waters`
--
ALTER TABLE `waters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `debits`
--
ALTER TABLE `debits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pumps`
--
ALTER TABLE `pumps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `waters`
--
ALTER TABLE `waters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `polnes`
--
CREATE DATABASE IF NOT EXISTS `polnes` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `polnes`;

-- --------------------------------------------------------

--
-- Table structure for table `alumnis`
--

CREATE TABLE `alumnis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_angkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumnis`
--

INSERT INTO `alumnis` (`id`, `nim`, `nama`, `jenis_kelamin`, `hp`, `jenjang`, `tahun_angkatan`, `judul`, `pembimbing_1`, `pembimbing_2`, `created_at`, `updated_at`) VALUES
(1, '2014.201.000012', 'Abdul Hamid Kurniawan2', 'Laki-laki', '0857528846482', 'D-IV2', '20142', 'Membangun blabla2', 'Nama112', 'Nama221', '2019-03-31 16:00:00', '2019-04-17 10:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `borangs`
--

CREATE TABLE `borangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenjang` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `butir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borangs`
--

INSERT INTO `borangs` (`id`, `jenjang`, `jenis`, `butir`, `judul`, `link`, `created_at`, `updated_at`) VALUES
(1, 'D3', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 17:43:42', '2019-04-14 20:09:18'),
(2, 'D3', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 09:58:54', '2019-04-14 20:09:24'),
(3, 'D3', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-20 04:53:20', '2019-03-20 04:53:20'),
(4, 'D3', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-20 04:54:48', '2019-03-20 04:54:48'),
(5, 'D3', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-20 04:56:00', '2019-03-20 04:56:00'),
(6, 'D3', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-20 04:56:56', '2019-03-20 04:57:04'),
(7, 'D3', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-20 04:57:53', '2019-03-20 04:57:53'),
(8, 'D3', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-20 04:58:46', '2019-03-20 04:58:46'),
(9, 'D3', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-20 04:59:32', '2019-03-20 04:59:32'),
(10, 'D3', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 21:47:53', '2019-03-20 21:47:53'),
(11, 'D3', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 21:48:48', '2019-03-20 21:48:48'),
(12, 'D3', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 21:49:19', '2019-03-20 21:49:19'),
(13, 'D3', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 21:49:59', '2019-03-20 21:49:59'),
(14, 'D3', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 21:50:26', '2019-03-20 21:50:26'),
(15, 'D3', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 21:50:57', '2019-03-20 21:50:57'),
(16, 'D3', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 21:51:28', '2019-03-20 21:51:28'),
(17, 'D3', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 21:52:12', '2019-03-20 21:52:12'),
(18, 'D3', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 21:53:01', '2019-03-20 21:53:01'),
(19, 'D3', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 21:53:32', '2019-03-20 21:53:32'),
(20, 'D3', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 21:54:02', '2019-03-20 21:54:02'),
(21, 'D3', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 21:54:24', '2019-03-20 21:54:24'),
(22, 'D3', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 21:55:03', '2019-03-20 21:55:03'),
(23, 'D3', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 21:55:36', '2019-03-20 21:55:36'),
(24, 'D3', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 21:56:11', '2019-03-20 21:56:11'),
(25, 'D3', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 21:56:49', '2019-03-20 21:56:49'),
(26, 'D3', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 21:57:17', '2019-03-20 21:57:17'),
(27, 'D3', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 21:57:47', '2019-03-20 21:57:47'),
(28, 'D3', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 21:58:09', '2019-03-20 21:58:09'),
(29, 'D3', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 21:58:36', '2019-03-20 21:58:36'),
(30, 'D3', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 21:59:11', '2019-03-20 21:59:11'),
(31, 'D3', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 22:00:02', '2019-03-20 22:00:02'),
(32, 'D3', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 22:00:28', '2019-03-20 22:00:28'),
(33, 'D3', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 22:01:09', '2019-03-20 22:01:09'),
(34, 'D3', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 22:01:35', '2019-03-20 22:01:35'),
(35, 'D3', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 22:02:00', '2019-03-20 22:02:00'),
(36, 'D3', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 22:02:20', '2019-03-20 22:02:20'),
(37, 'D3', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 22:02:42', '2019-03-20 22:02:42'),
(38, 'D3', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 22:03:08', '2019-03-20 22:03:08'),
(39, 'D3', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 22:03:34', '2019-03-20 22:03:34'),
(40, 'D3', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 22:03:55', '2019-03-20 22:03:55'),
(41, 'D3', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 22:04:42', '2019-04-15 00:01:36'),
(51, 'D4', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 09:43:42', '2019-04-14 12:09:18'),
(52, 'D4', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 01:58:54', '2019-04-14 12:09:24'),
(53, 'D4', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-19 20:53:20', '2019-03-19 20:53:20'),
(54, 'D4', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-19 20:54:48', '2019-03-19 20:54:48'),
(55, 'D4', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-19 20:56:00', '2019-03-19 20:56:00'),
(56, 'D4', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-19 20:56:56', '2019-03-19 20:57:04'),
(57, 'D4', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-19 20:57:53', '2019-03-19 20:57:53'),
(58, 'D4', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-19 20:58:46', '2019-03-19 20:58:46'),
(59, 'D4', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-19 20:59:32', '2019-03-19 20:59:32'),
(60, 'D4', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 13:47:53', '2019-03-20 13:47:53'),
(61, 'D4', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 13:48:48', '2019-03-20 13:48:48'),
(62, 'D4', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 13:49:19', '2019-03-20 13:49:19'),
(63, 'D4', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 13:49:59', '2019-03-20 13:49:59'),
(64, 'D4', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 13:50:26', '2019-03-20 13:50:26'),
(65, 'D4', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 13:50:57', '2019-03-20 13:50:57'),
(66, 'D4', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 13:51:28', '2019-03-20 13:51:28'),
(67, 'D4', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 13:52:12', '2019-03-20 13:52:12'),
(68, 'D4', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 13:53:01', '2019-03-20 13:53:01'),
(69, 'D4', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 13:53:32', '2019-03-20 13:53:32'),
(70, 'D4', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 13:54:02', '2019-03-20 13:54:02'),
(71, 'D4', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 13:54:24', '2019-03-20 13:54:24'),
(72, 'D4', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 13:55:03', '2019-03-20 13:55:03'),
(73, 'D4', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 13:55:36', '2019-03-20 13:55:36'),
(74, 'D4', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 13:56:11', '2019-03-20 13:56:11'),
(75, 'D4', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 13:56:49', '2019-03-20 13:56:49'),
(76, 'D4', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 13:57:17', '2019-03-20 13:57:17'),
(77, 'D4', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 13:57:47', '2019-03-20 13:57:47'),
(78, 'D4', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 13:58:09', '2019-03-20 13:58:09'),
(79, 'D4', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 13:58:36', '2019-03-20 13:58:36'),
(80, 'D4', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 13:59:11', '2019-03-20 13:59:11'),
(81, 'D4', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 14:00:02', '2019-03-20 14:00:02'),
(82, 'D4', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 14:00:28', '2019-03-20 14:00:28'),
(83, 'D4', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 14:01:09', '2019-03-20 14:01:09'),
(84, 'D4', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 14:01:35', '2019-03-20 14:01:35'),
(85, 'D4', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 14:02:00', '2019-03-20 14:02:00'),
(86, 'D4', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 14:02:20', '2019-03-20 14:02:20'),
(87, 'D4', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 14:02:42', '2019-03-20 14:02:42'),
(88, 'D4', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 14:03:08', '2019-03-20 14:03:08'),
(89, 'D4', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 14:03:34', '2019-03-20 14:03:34'),
(90, 'D4', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 14:03:55', '2019-03-20 14:03:55'),
(91, 'D4', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 14:04:42', '2019-04-14 16:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Profil', 'profil', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(2, 'Informasi Akademik', 'informasi-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(3, 'E-Alumni', 'e-alumni', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(4, 'Kalender Akademik', 'kalender-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(2, 'karyawan', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(3, 'mahasiswa', '2019-05-08 17:21:15', '2019-05-08 17:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE `jurusans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusans`
--

INSERT INTO `jurusans` (`id`, `jurusan`, `created_at`, `updated_at`) VALUES
(1, 'Teknik Listrik - DIII', '2019-05-08 17:21:19', '2019-05-08 17:21:19'),
(2, 'Teknik Listrik - DIV', '2019-05-08 17:21:19', '2019-05-08 17:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `nomor`, `judul`, `tujuan`, `alamat`, `nim`, `nama`, `email`, `hp`, `prodi`, `jenis`, `created_at`, `updated_at`) VALUES
(9, NULL, NULL, NULL, NULL, '2010.201.00011', 'Fakhor Abdurrahman', 'Fakhor2gmail.com', '085752884648', 'Teknik Listrik - DIII', 'Surat Aktif', '2019-05-09 21:30:04', '2019-05-09 21:30:04'),
(10, NULL, NULL, 'Sucofindo', NULL, '2010.204.00101', 'Albab Buchori', 'buchori@gmail.com', '08125827830', 'Teknik Listrik - DIV', 'Tugas Akhir', '2019-05-09 21:35:07', '2019-05-09 21:35:07'),
(22, NULL, 'Membangun D3', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Kerja Praktek - DIII', '2019-05-16 23:27:19', '2019-05-16 23:27:19'),
(23, NULL, 'Membangun D3', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Tugas Akhir', '2019-05-16 23:27:35', '2019-05-16 23:27:35'),
(24, NULL, 'Membangun D4', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Kerja Praktek - DIV', '2019-05-16 23:27:53', '2019-05-16 23:27:53'),
(25, NULL, 'Membangun D4', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Skripsi', '2019-05-16 23:28:10', '2019-05-16 23:28:10'),
(26, NULL, NULL, NULL, NULL, '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Surat Aktif', '2019-05-16 23:28:21', '2019-05-16 23:28:21'),
(27, NULL, NULL, NULL, NULL, '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Surat Aktif', '2019-05-16 23:28:36', '2019-05-16 23:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2019_02_25_112049_create_articles_table', 1),
(103, '2019_02_25_114415_create_tags_table', 1),
(104, '2019_02_25_114937_create_article_tag_table', 1),
(105, '2019_03_01_130823_create_categories_table', 1),
(106, '2019_03_01_131911_create_posts_table', 1),
(107, '2019_03_01_132215_create_comments_table', 1),
(108, '2019_03_07_145458_create_mails_table', 1),
(109, '2019_03_19_165935_create_borangs_table', 2),
(110, '2019_04_17_144710_create_alumnis_table', 3),
(111, '2019_05_08_011813_create_surat_aktif_table', 4),
(114, '2019_05_08_080441_create_jabatan_table', 5),
(115, '2019_05_08_080639_create_jurusan_table', 5),
(116, '2019_05_08_080441_create_jabatans_table', 6),
(121, '2019_05_09_011844_create_jabatans_table', 7),
(122, '2019_05_09_011901_create_jurusans_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 'judul cob', 'judul-cob', 'postspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostsposts', '2019-03-12 19:05:36', '2019-03-12 19:23:59'),
(2, 1, 'cat1', 'cat1', 'cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1', '2019-03-12 19:35:26', '2019-03-12 19:35:26'),
(3, 3, 'cat3', 'cat3', 'cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3', '2019-03-12 19:35:42', '2019-03-12 19:35:42'),
(4, 4, 'cat4', 'cat4', 'cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4', '2019-03-12 19:35:52', '2019-03-12 19:35:52'),
(5, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(6, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(7, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(8, 4, 'Kalender Akademik 2018 - 2019', 'cat4', '<p><img src=\"/photos/1/kalender akademik.jpg\" alt=\"\" width=\"1050\" height=\"525\" /></p>', '2019-03-12 21:35:52', '2019-03-13 20:24:20'),
(9, 1, 'Profil Jurusan Teknik Elektro', 'profil-jurusan-teknik-elektro', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/d3.jpg\" alt=\"\" width=\"800\" height=\"1000\" /></p>', '2019-03-13 03:45:10', '2019-03-13 18:36:39'),
(11, 2, 'judul cobaaa', 'judul-cobaaa', '<p style=\"text-align: left;\"><strong>sdasdasd</strong></p>', '2019-03-13 05:51:47', '2019-03-13 20:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `surat_aktif`
--

CREATE TABLE `surat_aktif` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_berita`
--

CREATE TABLE `tabel_berita` (
  `id` int(6) NOT NULL,
  `isi` varchar(5) NOT NULL,
  `gambar` varchar(5) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', '2019-03-04 10:01:44', '$2y$10$yvL.RUCDsRn2oUhPwQkZoOC59TG9i9lOvqGOOcsFcCbM4UYJFFKby', 'AkBDnDpNM1EnVqadt63JwZjLOye96ZAx0LJ4cMOt87Ugf7hLf9JDcHHqjSeJ', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(5, 'Erry Yadie', 'erryyadie@polnes.ac.id', 'karyawan', '2019-04-05 00:41:26', '$2y$10$1lMGYZeMmiqbSJUpcj0b2.giS09RFsvhxm7orsastXBfu3yI7Xjm6', 'hpFjqWdAwg4lIDpktKoc8iN9Gg0jgIt2Qg0kaPMmS4GXG6HRPgRtECMpAKkB', '2019-04-05 00:31:14', '2019-05-08 17:26:48'),
(6, 'mhs', 'mhs@mhs.com', 'mahasiswa', '2019-04-05 00:41:26', '$2y$10$itC5HFR6xH/9I/IPzCPgwejmff7N8eHqU2bItGPQRbJfjYprxUeEG', NULL, '2019-05-07 21:38:08', '2019-05-07 21:38:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnis`
--
ALTER TABLE `alumnis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borangs`
--
ALTER TABLE `borangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusans`
--
ALTER TABLE `jurusans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumnis`
--
ALTER TABLE `alumnis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `borangs`
--
ALTER TABLE `borangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jurusans`
--
ALTER TABLE `jurusans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
--
-- Database: `sia`
--
CREATE DATABASE IF NOT EXISTS `sia` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sia`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stok`
--

CREATE TABLE `tbl_stok` (
  `id_barang` int(4) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `stok_barang` int(3) NOT NULL,
  `jenis_barang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_stok`
--

INSERT INTO `tbl_stok` (`id_barang`, `nama_barang`, `stok_barang`, `jenis_barang`) VALUES
(1, 'Sabun Cair', 10, 'Pcs'),
(2, 'Pasta Gigi', 0, 'Kotak'),
(3, 'Gula', 0, 'Pcs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  ADD PRIMARY KEY (`id_barang`);
--
-- Database: `spm`
--
CREATE DATABASE IF NOT EXISTS `spm` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `spm`;

-- --------------------------------------------------------

--
-- Table structure for table `backset`
--

CREATE TABLE `backset` (
  `url` varchar(100) NOT NULL,
  `sessiontime` varchar(4) DEFAULT NULL,
  `footer` varchar(50) DEFAULT NULL,
  `themesback` varchar(2) DEFAULT NULL,
  `responsive` varchar(2) DEFAULT NULL,
  `haha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backset`
--

INSERT INTO `backset` (`url`, `sessiontime`, `footer`, `themesback`, `responsive`, `haha`) VALUES
('http://localhost/spm', '', '', '3', '0', '2017-01-20 07:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `chmenu`
--

CREATE TABLE `chmenu` (
  `userkabkot` varchar(20) NOT NULL,
  `menu1` varchar(1) DEFAULT '0',
  `menu2` varchar(1) DEFAULT '0',
  `menu3` varchar(1) DEFAULT '0',
  `menu4` varchar(1) DEFAULT '0',
  `menu5` varchar(1) DEFAULT '0',
  `menuprov` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chmenu`
--

INSERT INTO `chmenu` (`userkabkot`, `menu1`, `menu2`, `menu3`, `menu4`, `menu5`, `menuprov`) VALUES
('admin', '5', '5', '5', '5', '5', '5'),
('balikpapan', '0', '0', '5', '5', '5', '0'),
('Provinsi', '5', '5', '0', '0', '0', '5'),
('Samarinda', '0', '0', '5', '5', '5', '0');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `nama` varchar(100) DEFAULT NULL,
  `tagline` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `notelp` varchar(20) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`nama`, `tagline`, `alamat`, `notelp`, `signature`, `avatar`, `no`) VALUES
('-', '-', '-', '-', '-', 'dist/upload/index.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `nama` varchar(50) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `isi` text,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`nama`, `avatar`, `tanggal`, `isi`, `id`) VALUES
('admin', 'dist/upload/logobarber.png', '2018-03-21', '<p>Berita Informasi<br></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kabkot`
--

CREATE TABLE `kabkot` (
  `kode` varchar(20) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabkot`
--

INSERT INTO `kabkot` (`kode`, `nama`, `no`) VALUES
('K0001', 'admin', 28),
('K0002', 'Provinsi', 100),
('K0003', 'Samarinda', 30),
('K0004', 'balikpapan', 31);

-- --------------------------------------------------------

--
-- Table structure for table `realisasi`
--

CREATE TABLE `realisasi` (
  `kode` varchar(10) NOT NULL,
  `kabkot` varchar(25) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `triwulan` varchar(15) NOT NULL,
  `ibuhamil` int(2) NOT NULL,
  `ibubersalin` int(2) NOT NULL,
  `bayilahir` int(2) NOT NULL,
  `balita` int(2) NOT NULL,
  `upd` int(2) NOT NULL,
  `produktif` int(2) NOT NULL,
  `lanjut` int(2) NOT NULL,
  `hipertensi` int(2) NOT NULL,
  `dm` int(2) NOT NULL,
  `odgjb` int(2) NOT NULL,
  `tb` int(2) NOT NULL,
  `hiv` int(2) NOT NULL,
  `permasalahan1` varchar(225) NOT NULL,
  `permasalahan2` varchar(225) NOT NULL,
  `permasalahan3` varchar(225) NOT NULL,
  `permasalahan4` varchar(225) NOT NULL,
  `permasalahan5` varchar(225) NOT NULL,
  `permasalahan6` varchar(225) NOT NULL,
  `permasalahan7` varchar(225) NOT NULL,
  `permasalahan8` varchar(225) NOT NULL,
  `permasalahan9` varchar(225) NOT NULL,
  `permasalahan10` varchar(225) NOT NULL,
  `permasalahan11` varchar(225) NOT NULL,
  `permasalahan12` varchar(225) NOT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `realisasi`
--

INSERT INTO `realisasi` (`kode`, `kabkot`, `tahun`, `triwulan`, `ibuhamil`, `ibubersalin`, `bayilahir`, `balita`, `upd`, `produktif`, `lanjut`, `hipertensi`, `dm`, `odgjb`, `tb`, `hiv`, `permasalahan1`, `permasalahan2`, `permasalahan3`, `permasalahan4`, `permasalahan5`, `permasalahan6`, `permasalahan7`, `permasalahan8`, `permasalahan9`, `permasalahan10`, `permasalahan11`, `permasalahan12`, `no`) VALUES
('R0001', 'Samarinda', '2019', 'Triwulan II', 10, 8, 10, 10, 10, 10, 10, 19, 10, 10, 10, 10, '', 'data masih di cari', '', '', '', '', '', 'data masih di hitung', '', '', '', '', 9),
('R0002', 'balikpapan', '2019', 'Triwulan I', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, '', '', '', '', '', '', '', '', '', '', '', '', 10),
('R0003', 'Samarinda', '2019', 'Triwulan I', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, '', '', '', '', '', '', '', '', '', '', '', '', 12),
('R0004', 'Samarinda', '2019', 'Triwulan III', 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, '', '', '', '', '', '', '', '', '', '', '', '', 13),
('R0005', 'balikpapan', '2019', 'Triwulan II', 5, 4, 3, 4, 5, 5, 5, 4, 4, 4, 5, 10, 'hahah', 'ahah', 'hahah', 'ahah', 'ha', 'ah', 'aha', 'haha', 'h', 'haah', 'haah', '', 14),
('R0006', 'Samarinda', '2019', 'Triwulan IV', 10, 10, 10, 9, 10, 10, 10, 10, 10, 10, 10, 0, '', '', '', '', '', '', '', '', '', '', '', 'tidak ada', 15),
('R0007', 'balikpapan', '2019', 'Triwulan III', 8, 8, 8, 8, 8, 8, 5, 5, 5, 8, 8, 8, '', '', '', '', '', '', 'belum di cek', 'belum di cek', 'belum di cek', '', '', '', 16);

-- --------------------------------------------------------

--
-- Table structure for table `sasaran`
--

CREATE TABLE `sasaran` (
  `kode` varchar(25) NOT NULL,
  `ibuhamil` int(2) NOT NULL,
  `ibubersalin` int(2) NOT NULL,
  `bayilahir` int(2) NOT NULL,
  `balita` int(2) NOT NULL,
  `upd` int(2) NOT NULL,
  `produktif` int(2) NOT NULL,
  `lanjut` int(2) NOT NULL,
  `hipertensi` int(2) NOT NULL,
  `dm` int(2) NOT NULL,
  `odgjb` int(2) NOT NULL,
  `tb` int(2) NOT NULL,
  `hiv` int(2) NOT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sasaran`
--

INSERT INTO `sasaran` (`kode`, `ibuhamil`, `ibubersalin`, `bayilahir`, `balita`, `upd`, `produktif`, `lanjut`, `hipertensi`, `dm`, `odgjb`, `tb`, `hiv`, `no`) VALUES
('Balikpapan', 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 11),
('Samarinda', 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userna_me` varchar(20) NOT NULL,
  `pa_ssword` varchar(70) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nohp` varchar(20) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `tglaktif` date DEFAULT NULL,
  `kabkot` varchar(20) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userna_me`, `pa_ssword`, `nama`, `alamat`, `nohp`, `tgllahir`, `tglaktif`, `kabkot`, `avatar`, `no`) VALUES
('admin', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 'admin', 'jl jalan', '0875757777', '2018-03-21', '2018-03-03', 'admin', 'dist/upload/no-image.jpg', 1),
('balikpapan', '490a526a47ae38ff0c9e9467a8de6920b7532e79', 'balikpapan', '-', '-', '2019-07-01', '2019-07-23', 'balikpapan', 'dist/upload/index.jpg', 4),
('gerry', 'b54c32f29d457c4ab13ad3b4f0d553a149d4c0e9', 'Gerry Chrisna Perkasa Putra', '-', '-', '2019-07-01', '2019-07-01', 'Provinsi', 'dist/upload/f42b6086-522d-4fe2-a7a7-023a682a8553.png', 13),
('provinsi', '1b0b2b22dc5d12a0f427fc0e3ade2539cf965bad', 'provinsi', '-', '-', '2019-07-01', '2019-07-01', 'Provinsi', 'dist/upload/index.jpg', 2),
('samarinda', '3c2218264c24f36a719b0b2d585b1f5ab37c81ba', 'samarinda', '-', '-', '2019-07-01', '2019-07-23', 'Samarinda', 'dist/upload/index.jpg', 3),
('wawan', 'da010dc2e5cb0f8c101e46184dc0658c32ac8eaa', 'wawan1', 'sss', 'aaaaaaaaaaaaa', '2020-02-20', '2020-02-03', 'Samarinda', 'dist/upload/index.jpg', 14);

-- --------------------------------------------------------

--
-- Table structure for table `verifikasi`
--

CREATE TABLE `verifikasi` (
  `kode` varchar(10) NOT NULL,
  `r1` int(1) NOT NULL,
  `r2` int(1) NOT NULL,
  `r3` int(1) NOT NULL,
  `r4` int(1) NOT NULL,
  `r5` int(1) NOT NULL,
  `r6` int(1) NOT NULL,
  `r7` int(1) NOT NULL,
  `r8` int(1) NOT NULL,
  `r9` int(1) NOT NULL,
  `r10` int(1) NOT NULL,
  `r11` int(1) NOT NULL,
  `r12` int(1) NOT NULL,
  `no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verifikasi`
--

INSERT INTO `verifikasi` (`kode`, `r1`, `r2`, `r3`, `r4`, `r5`, `r6`, `r7`, `r8`, `r9`, `r10`, `r11`, `r12`, `no`) VALUES
('R0001', 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 10),
('R0002', 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 11),
('R0003', 2, 2, 1, 1, 1, 2, 2, 1, 2, 2, 2, 2, 12),
('R0004', 2, 2, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 13),
('R0005', 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 14),
('R0006', 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 15),
('R0007', 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 16);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backset`
--
ALTER TABLE `backset`
  ADD PRIMARY KEY (`url`);

--
-- Indexes for table `chmenu`
--
ALTER TABLE `chmenu`
  ADD PRIMARY KEY (`userkabkot`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD KEY `id` (`id`);

--
-- Indexes for table `kabkot`
--
ALTER TABLE `kabkot`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `no` (`no`);

--
-- Indexes for table `realisasi`
--
ALTER TABLE `realisasi`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `no3` (`no`);

--
-- Indexes for table `sasaran`
--
ALTER TABLE `sasaran`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `no3` (`no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userna_me`),
  ADD KEY `no` (`no`);

--
-- Indexes for table `verifikasi`
--
ALTER TABLE `verifikasi`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `no3` (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kabkot`
--
ALTER TABLE `kabkot`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `realisasi`
--
ALTER TABLE `realisasi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sasaran`
--
ALTER TABLE `sasaran`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `verifikasi`
--
ALTER TABLE `verifikasi`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Database: `tefasmk`
--
CREATE DATABASE IF NOT EXISTS `tefasmk` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tefasmk`;

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `terjual` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penjual` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promosi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `nama`, `kategori`, `harga`, `stok`, `terjual`, `penjual`, `deskripsi`, `video`, `promosi`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Roti Arnon', 'Roti Kering', '18000', 8, NULL, 'admin', 'sadasdsadasdasd', 'ss', 'ss', NULL, '2019-12-07 18:56:01', '2019-12-07 19:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `beritas`
--

CREATE TABLE `beritas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `beritas`
--

INSERT INTO `beritas` (`id`, `judul`, `isi`, `jenis`, `gambar`, `user`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum', '<h1>Lorem Ipsum</h1>\r\n\r\n<p>&quot;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...&quot;</p>\r\n\r\n<p>&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</p>\r\n\r\n<hr />\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Where does it come from?</h2>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n\r\n<h2>Where can I get some?</h2>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 'home', '132594443.jpg', 'admin', '2020-02-04 17:23:41', '2020-02-04 19:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idbarang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `penjual` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user`, `idbarang`, `nama`, `kategori`, `harga`, `jumlah`, `penjual`, `created_at`, `updated_at`) VALUES
(26, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 1, 'admin', '2019-12-09 23:55:37', '2019-12-09 23:55:37'),
(27, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 2, 'admin', '2019-12-09 23:55:39', '2019-12-09 23:55:39'),
(28, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 3, 'admin', '2019-12-09 23:55:42', '2019-12-09 23:55:42'),
(29, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 3, 'admin', '2019-12-09 23:55:43', '2019-12-09 23:55:43'),
(30, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 4, 'admin', '2019-12-09 23:55:45', '2019-12-09 23:55:45'),
(31, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 5, 'admin', '2019-12-09 23:55:46', '2019-12-09 23:55:46'),
(32, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 6, 'admin', '2019-12-09 23:55:48', '2019-12-09 23:55:48'),
(33, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 7, 'admin', '2019-12-09 23:55:49', '2019-12-09 23:55:49'),
(34, 'admin', '1', 'Roti Arnon', 'Roti Kering', '18000', 8, 'admin', '2019-12-09 23:55:50', '2019-12-09 23:55:50');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stop` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `kategori`, `deskripsi`, `start`, `stop`, `foto`, `video`, `created_at`, `updated_at`) VALUES
(1, 'Roti Basah', '', '', '', '', '', NULL, NULL),
(2, 'Roti Kering', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `debits`
--

CREATE TABLE `debits` (
  `id` int(10) UNSIGNED NOT NULL,
  `debit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idbarang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `nama`, `idbarang`, `created_at`, `updated_at`) VALUES
(5, 'admin_677631872.jpg', '1', '2019-12-07 20:12:35', '2019-12-07 20:12:35'),
(6, 'admin_438910703.jpg', '1', '2019-12-07 20:12:35', '2019-12-07 20:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(16, '2019_12_05_042236_create_imagess_table', 5),
(52, '2014_10_12_000000_create_users_table', 6),
(53, '2014_10_12_100000_create_password_resets_table', 6),
(54, '2019_08_19_000000_create_failed_jobs_table', 6),
(55, '2019_11_12_154812_create_debits_table', 6),
(56, '2019_11_12_162907_create_pumps_table', 6),
(57, '2019_11_13_160912_create_jabatans_table', 6),
(58, '2019_11_13_160955_create_waters_table', 6),
(59, '2019_12_04_053706_create_barangs_table', 6),
(60, '2019_12_04_054216_create_categories_table', 6),
(61, '2019_12_04_054306_create_penjualans_table', 6),
(62, '2019_12_05_042324_create_images_table', 6),
(64, '2019_12_08_171747_create_carts_table', 7),
(65, '2020_02_01_093436_create_beritas_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `penjualans`
--

CREATE TABLE `penjualans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `pembeli` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `penjual` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pumps`
--

CREATE TABLE `pumps` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', 'admin', NULL, '$2y$10$cagXjHSLR/xAD15eVHgfI.hNrlN6DYIlPGOSJPdXDwyHloSY05yG2', NULL, '2019-12-07 18:54:13', '2019-12-07 18:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `waters`
--

CREATE TABLE `waters` (
  `id` int(10) UNSIGNED NOT NULL,
  `usage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `penjualans`
--
ALTER TABLE `penjualans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pumps`
--
ALTER TABLE `pumps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_jabatan_unique` (`jabatan`);

--
-- Indexes for table `waters`
--
ALTER TABLE `waters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `debits`
--
ALTER TABLE `debits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `penjualans`
--
ALTER TABLE `penjualans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pumps`
--
ALTER TABLE `pumps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `waters`
--
ALTER TABLE `waters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'hamid', '123456', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Database: `tim17`
--
CREATE DATABASE IF NOT EXISTS `tim17` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `tim17`;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'hamid', 'wawan@gmail.com', NULL, '$2y$10$fxkeEFWYLfTt6F93eNfU5espqBsb1tIrv5pOV1F8dMuMW1MaqZDom', NULL, '2019-08-03 01:31:37', '2019-08-03 01:31:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
