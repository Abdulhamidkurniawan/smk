<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Teaching Factory SMK 2 PPU</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />

  <!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

	<!-- Animate.css')}} -->
	<link rel="stylesheet" href="{{asset('css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{asset('css/flexslider.css')}}">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

	<!-- Date Picker -->
	<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="{{asset('fonts/flaticon/font/flaticon.css')}}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{asset('css/style.css')}}">

	<script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<![endif]-->

	</head>
	<body>
            <div id="page">
                    <nav class="colorlib-nav" role="navigation">
                        <div class="top-menu">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div id="colorlib-logo"> <img src="{{asset('images/smkn2.png')}}" onclick="{{ route('index') }}" height="60" width="50"><a href="{{ route('index') }}"> TEFA SMKN2 PPU</a></div>
                                    </div>
                                    <div class="col-xs-8 text-right menu-1">
                                        <ul>
                                            <li><a href="{{ route('index') }}" style="line-height: 39px;">Home</a></li>
                                            <li class="has-dropdown">
                                                <a href="{{ route('shop') }}">Shop</a>
                                                {{-- <ul class="dropdown">
                                                    <li><a href="{{ route('checkout') }}">Checkout</a></li>
                                                    <li><a href="{{ route('order') }}">Order Complete</a></li>
                                                    <li><a href="{{ route('wishlist') }}">Wishlist</a></li>
                                                </ul> --}}
                                            </li>
                                            <li><a href="{{ route('blog') }}">Blog</a></li>
                                            <li><a href="http://www.smkn2ppu.sch.id">About</a></li>
                                            <li><a href="http://www.smkn2ppu.sch.id/home/hubungi_kami">Contact</a></li>

                                            {{-- <li><a href="{{ route('about') }}">About</a></li>
                                            <li><a href="{{ route('contact') }}">Contact</a></li> --}}

                                            @guest
                                            <li><a href="{{ route('login') }}">Login</a></li>
                                            @if (Route::has('register'))
                                            <li><a href="{{ route('register') }}">Register</a></li>
                                            @endif
                                        @else
                                            <li class="active"><a href="{{ route('home.cart') }}"><i class="icon-shopping-cart"></i> Cart</a></li>
                                            <li class="has-dropdown">
                                                <a href="{{ route('dashboard') }}">{{ Auth::user()->name }}</a>
                                                <ul class="dropdown">
                                                    <li><a href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                      document.getElementById('logout-form').submit();">Logout</a></li>
                                                </ul>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                            </li>
                                        @endguest

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
            @yield('content')

            <footer id="colorlib-footer" role="contentinfo">
                <div class="container">
                    <div class="row row-pb-md">
                        <div class="col-md-3 colorlib-widget">
                            <h4>About Store</h4>
                            <p>Program Teaching Factory adalah suatu konsep pembelajaran di SMK berbasis produksi atau jasa yang mengacu kepada standar dan prosedur yang berlaku di industri.</p>
                            <p>
                                <ul class="colorlib-social-icons">
                                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                    <li><a href="#"><i class="icon-dribbble"></i></a></li>
                                </ul>
                            </p>
                        </div>
                        <div class="col-md-2 colorlib-widget">
                            <h4>Shop</h4>
                            <p>
                                <ul class="colorlib-footer-links">
                                    <li><a href="{{ route('shop') }}">Product</a></li>
                                    {{-- <li><a href="#">Returns/Exchange</a></li>
                                    <li><a href="#">Gift Voucher</a></li>
                                    <li><a href="#">Wishlist</a></li>
                                    <li><a href="#">Special</a></li>
                                    <li><a href="#">Customer Services</a></li>
                                    <li><a href="#">Site maps</a></li> --}}
                                </ul>
                            </p>
                        </div>
                        <div class="col-md-2 colorlib-widget">
                            <h4>Information</h4>
                            <p>
                                <ul class="colorlib-footer-links">
                                    <li><a href="http://www.smkn2ppu.sch.id/">About us</a></li>
                                    <li><a href="http://www.smkn2ppu.sch.id/home/hubungi_kami">Contact Us</a></li>
                                    {{-- <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Support</a></li>
                                    <li><a href="#">Order Tracking</a></li> --}}
                                </ul>
                            </p>
                        </div>

                        <div class="col-md-2">
                            <h4>News</h4>
                            <ul class="colorlib-footer-links">
                                <li><a href="{{ route('blog') }}">Blog</a></li>
                                {{-- <li><a href="#">Press</a></li>
                                <li><a href="#">Exhibitions</a></li> --}}
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <h4>Contact Information</h4>
                            <ul class="colorlib-footer-links">
                                <li>Alamat : Jl.Provinsi Km.8 Nipah-Nipah, Penajam Paser Utara, Kalimantan Timur</li>
                                <li><a href="tel://+625427211392">0542-7211392</a></li>
                                {{-- <li><a href="mailto:info@yoursite.com">info@yoursite.com</a></li> --}}
                                <li><a href="#">tefa.skadappu.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{-- <div class="copy">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>
                                <span class="block"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart2" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
                                <span class="block">Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a> , <a href="http://pexels.com/" target="_blank">Pexels.com</a></span>
                            </p>
                        </div>
                    </div>
                </div> --}}
            </footer>
            <div class="gototop js-top">
                <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
            </div>

	<!-- jQuery -->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
	<!-- Flexslider -->
	<script src="{{asset('js/jquery.flexslider-min.js')}}"></script>
	<!-- Owl carousel -->
	<script src="{{asset('js/owl.carousel.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('js/magnific-popup-options.js')}}"></script>
	<!-- Date Picker -->
	<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
	<!-- Stellar Parallax -->
	<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="{{asset('js/google_map.js')}}"></script>
	<!-- Main -->
	<script src="{{asset('js/main.js')}}"></script>
    </body>
</html>
