@extends('layouts.home')
@section('content')

		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
                    <li style="background-image: url({!! url("images/smkbg.png") !!});">
                        <div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Our Blog</h1>
				   					<h2 class="bread"><span><a href="index.html">Home</a></span> <span>Blog</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-blog">
			<div class="container">
				<div class="row">
                    @foreach ($beritas as $berita)
					<div class="col-md-4">
						<article class="article-entry">
							<a href="{{ route('blogdetail', $berita) }}" class="blog-img" style="background-image: url(../../tefa/upload/berita/{{$berita->gambar}})"></a>
							<div class="desc">
								<p class="meta"><span class="date">{!! Carbon\Carbon::parse($berita->created_at)->format('j M') !!}</span><span class="month">{!! Carbon\Carbon::parse($berita->created_at)->format('Y') !!}</span></p>
								<p class="admin"><span>Posted by:</span> <span>{{$berita->user}}</span></p>
								<h2><a href="{{ route('blogdetail', $berita) }}">{{$berita->judul}}</a></h2>
								<p>{{Str::limit($berita->isi, 190, '...')}}</p>
							</div>
						</article>
					</div>
                    @endforeach
                    <div class="col-md-12" align="center">
                        {!! $beritas->render() !!}
                </div>
				</div>
			</div>
		</div>
<!--
		<div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-6 text-center">
							<h2><i class="icon-paperplane"></i>Sign Up for a Newsletter</h2>
						</div>
						<div class="col-md-6">
							<form class="form-inline qbstp-header-subscribe">
								<div class="row">
									<div class="col-md-12 col-md-offset-0">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Enter your email">
											<button type="submit" class="btn btn-primary">Subscribe</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->

    @stop


