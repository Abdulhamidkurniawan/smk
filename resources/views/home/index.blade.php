@extends('layouts.home')
@section('content')

		<aside id="colorlib-hero">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url(images/bg1.jpeg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
				   				{{-- <div class="slider-text-inner">
				   					<div class="desc">
					   					<h1 class="head-1">Men's</h1>
					   					<h2 class="head-2">Jeans</h2>
					   					<h2 class="head-3">Collection</h2>
					   					<p class="category"><span>New stylish shirts, pants &amp; Accessories</span></p>
					   					<p><a href="#" class="btn btn-primary">Shop Collection</a></p>
				   					</div>
				   				</div> --}}
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/bg2.jpeg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="desc">
					   					<h1 class="head-1">Huge</h1>
					   					<h2 class="head-2">Sale</h2>
					   					<h2 class="head-3">45% off</h2>
					   					<p class="category"><span>Fresh From The Oven</span></p>
					   					<p><a href="{{ route('shop') }}" class="btn btn-primary">Shop Now!!!</a></p>
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			   	<li style="background-image: url(images/bg3.jpeg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-push-4 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="desc">
					   					<h1 class="head-1">TEFA</h1>
					   					<h2 class="head-2">SMKN2 </h2>
					   					<h2 class="head-3">PPU</h2>
					   					<p class="category"><span>Produk Hasil Karya Siswa/i</span></p>
					   					<p><a href="{{ route('shop') }}" class="btn btn-primary">Shop Now!!!</a></p>
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>
		<div id="colorlib-featured-product">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Produk Terlaris</span></h2>
						<p>Temukan Produk Terlaris yang paling banyak dicari.</p>
					</div>
				</div>
				<div class="row">
                    @foreach ($bestsellers as $bs)
					<div class="col-md-3 text-center">
						<div class="product-entry" onclick="location.href='{{ route('detail', $bs) }}';">
                            <div class="product-img" style="background-image: url(product/{!! App\Images::where('idbarang','=',$bs->id)->limit(1)->value('nama');!!})">
								<p class="tag"><span class="new">New</span></p>
                                <div class="cart">
                                    <p>
                                            <span><a href="{{ route('detail', $bs) }}"><i class="icon-eye"></i></a></span>
                                            <span class="addtocart"><a href="{{ route('home.cart') }}"><i class="icon-shopping-cart"></i></a></span>
                                    </p>
                                </div>
							</div>
                            <div class="desc">
                                <h3><a href="{{ route('detail', $bs) }}">{{$bs->nama}}</a></h3>
                                <p class="price"><span>Rp.{{$bs->harga}}</span></p>
                            </div>
						</div>
                    </div>
                    @endforeach
				</div>
			</div>
		</div>
		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Produk Terbaru</span></h2>
						<p>Temukan Produk Terbaru dari siswa/i SMK Negeri 2 Penajam Paser Utara.</p>
					</div>
				</div>
				<div class="row">
                    @foreach ($barus as $baru)
					<div class="col-md-3 text-center">
						<div class="product-entry" onclick="location.href='{{ route('detail', $baru) }}';">
                            <div class="product-img" style="background-image: url(product/{!! App\Images::where('idbarang','=',$baru->id)->limit(1)->value('nama');!!})">
								<p class="tag"><span class="new">New</span></p>
                                <div class="cart">
                                    <p>
                                            <span><a href="{{ route('detail', $baru) }}"><i class="icon-eye"></i></a></span>
                                            <span class="addtocart"><a href="{{ route('home.cart') }}"><i class="icon-shopping-cart"></i></a></span>
                                    </p>
                                </div>
							</div>
                            <div class="desc">
                                <h3><a href="{{ route('detail', $baru) }}">{{$baru->nama}}</a></h3>
                                <p class="price"><span>Rp.{{$baru->harga}}</span></p>
                            </div>
						</div>
                    </div>
                    @endforeach
				</div>
			</div>
        </div>
        @foreach ($beritas as $berita)
		<div id="colorlib-intro" class="colorlib-intro" style="background-image: url(upload/berita/{!!$berita->gambar;!!})" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="intro-desc">
							<div class="text-salebox">
								<div class="text-lefts">
									{{-- <div class="sale-box">
										<div class="sale-box-top">
											<h2 class="number">45</h2>
											<span class="sup-1">%</span>
											<span class="sup-2">Off</span>
										</div>
										<h2 class="text-sale">Sale</h2>
									</div> --}}
								</div>
								<div class="text-rights">
                                <h3 class="title">{{$berita->judul}}</h3>
									<p>{!!Str::limit($berita->isi, 200, '...')!!}</p>
                                    <p><a href="{{ route('shop') }}" class="btn btn-primary">Shop Now</a> <a href="{{ route('blogdetail', $berita) }}" class="btn btn-primary btn-outline">Read more</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        @endforeach
	</div>


    @stop



