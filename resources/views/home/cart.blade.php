@extends('layouts.home')
@section('content')

		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
                    <li style="background-image: url({!! url("images/smkbg.png") !!});">
                        <div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Shopping Cart</h1>
				   					<h2 class="bread"><span><a href="index.html">Home</a></span> <span><a href="shop.html">Product</a></span> <span>Shopping Cart</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-10 col-md-offset-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<h3>Shopping Cart</h3>
							</div>
							<div class="process text-center">
								<p><span>02</span></p>
								<h3>Checkout</h3>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<h3>Order Complete</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-10 col-md-offset-1">
						<div class="product-name">
							<div class="one-forth text-center">
								<span>Product Details</span>
							</div>
							<div class="one-eight text-center">
								<span>Price</span>
							</div>
							<div class="one-eight text-center">
								<span>Quantity</span>
							</div>
							<div class="one-eight text-center">
								<span>Total</span>
							</div>
							<div class="one-eight text-center">
								<span>Remove</span>
							</div>
                        </div>
                        <?php
                        $ttl=0;
                        $gt=0;     ?>
                        @foreach ($carts as $cart)
						<div class="product-cart">
							<div class="one-forth">
								<div class="product-img" style="background-image: url(product/{!! App\Images::where('idbarang','=',$cart->idbarang)->limit(1)->value('nama');!!})">
								</div>
								<div class="display-tc">
									<h3>{{$cart->nama}}</h3>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">Rp.{{$cart->harga}}</span>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									{{-- <input type="text" id="quantity" name="quantity" class="form-control input-number text-center" value="1" min="1" max="100"> --}}
									<span class="price">{{$cart->jumlah}}</span>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">Rp.<?php echo $ttl=($cart->harga*$cart->jumlah); ?>
                                    </span>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
                                        <form id="remove" action="{{ route('cart.destroy', $cart) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <a href="#" class="closed" onclick="document.getElementById('remove').submit()"></a>
                                        </form>
								</div>
							</div>
                        </div>
                        <?php $gt=$gt+$ttl; ?>
                        @endforeach
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="total-wrap">
							<div class="row">
								<div class="col-md-8">
                                        {{-- <form action="#">
                                                <div class="row form-group">
                                                    <div class="col-md-9">
                                                        <input type="text" name="quantity" class="form-control input-number" placeholder="Your Coupon Number...">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="submit" value="Apply Coupon" class="btn btn-primary">
                                                    </div>
                                                </div>
                                            </form> --}}
								</div>
								<div class="col-md-3 col-md-push-1 text-center">
									<div class="total">
										<div class="sub">
											{{-- <p><span>Subtotal:</span> <span>$200.00</span></p>
											<p><span>Delivery:</span> <span>$0.00</span></p>
											<p><span>Discount:</span> <span>$45.00</span></p> --}}
										</div>
										<div class="grand-total">
                                            <p><strong>Total:</strong></span><span>Rp.<?php echo $gt;?></p>
                                                <form action="{{ route('cart.checkout')}}" method="post">
                                                    {{ csrf_field() }}
                                                    <p><input type="submit" value="Order" class="btn btn-primary"></p>
                                            </form>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>

		<!-- <div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-6 text-center">
							<h2><i class="icon-paperplane"></i>Sign Up for a Newsletter</h2>
						</div>
						<div class="col-md-6">
							<form class="form-inline qbstp-header-subscribe">
								<div class="row">
									<div class="col-md-12 col-md-offset-0">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Enter your email">
											<button type="submit" class="btn btn-primary">Subscribe</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->

    @stop


