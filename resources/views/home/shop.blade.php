@extends('layouts.home')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
                    <li style="background-image: url({!! url("images/smkbg.png") !!});">
                        <div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Products</h1>
				   					<h2 class="bread"><span><a href="index.html">Home</a></span> <span>Shop</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-push-2">
						<div class="row row-pb-lg">
                            @foreach ($barangs as $barang)
							<div class="col-md-4 text-center">
								<div class="product-entry" onclick="location.href='{{ route('detail', $barang) }}';">
									<div class="product-img" style="background-image: url(product/{!! App\Images::where('idbarang','=',$barang->id)->limit(1)->value('nama');!!})">
										{{-- <p class="tag"><span class="new">New</span></p> --}}
										<div class="cart">
											<p>
                                                    <span><a href="{{ route('detail', $barang) }}"><i class="icon-eye"></i></a></span>
                                                    <span class="addtocart"><a href="{{ route('home.cart') }}"><i class="icon-shopping-cart"></i></a></span>
											</p>
										</div>
									</div>
									<div class="desc">
										<h3><a href="{{ route('detail', $barang) }}">{{$barang->nama}}</a></h3>
										<p class="price"><span>Rp.{{$barang->harga}}</span></p>
									</div>
								</div>
                            </div>
                        @endforeach
                        <div class="col-md-12" align="center">
                            {!! $barangs->render() !!}
                    </div>
						</div>
					</div>
					<div class="col-md-2 col-md-pull-10">
						<div class="sidebar">
							<div class="side">
                                <h4>Filter</h4>
                                <form method="GET" action="{{ route('shop') }}">
                                    <!-- @csrf -->
									<?php 	$filter = substr("$_SERVER[QUERY_STRING]",8);
											$length = strlen($filter);
											// echo $filter,$length;
											?>
                                    <select id=urutkan name="urutkan" onchange="this.form.submit();">
                                        <option value="">Pilih Filter</option>
                                        <option value="terbaru"<?php if ($sort=="terbaru"){echo "selected";}?>>Terbaru</option>
                                        <option value="tertinggi"<?php if ($sort=="tertinggi"){echo "selected";}?>>Harga Tertinggi</option>
                                        <option value="terendah"<?php if ($sort=="terendah"){echo "selected";}?>>Harga Terendah</option>
                                    </select>
                                </form>
                                <p></p>
								<div class="fancy-collapse-panel">
			                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			                     <div class="panel panel-default">
			                         <div class="panel-heading" role="tab" id="headingOne">
			                             <h4 class="panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Kategori
                                                    </a>
			                             </h4>
			                         </div>
			                         <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			                             <div class="panel-body">
			                                 <ul>
                                                @foreach ($kategoris as $kategori)
												<?php if ($length > 0 and $length < 10) { ?>
													<li><a href="{{ route('shop','kategori='.$kategori->kategori.'&urutkan='.$filter) }}" id="kategori">{{$kategori->kategori}}</a></li>
												<?php } else { ?>
													<li><a href="{{ route('shop','kategori='.$kategori->kategori) }}" id="kategori">{{$kategori->kategori}}</a></li>
												<?php }?>
                                                 @endforeach
                                                </ul>
			                             </div>
			                         </div>
			                     </div>

			                     </div>
			                  </div>
			               </div>
							</div>
						</div>
					</div>
				</div>
			</div>
<!--
		<div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-6 text-center">
							<h2><i class="icon-paperplane"></i>Sign Up for a Newsletter</h2>
						</div>
						<div class="col-md-6">
							<form class="form-inline qbstp-header-subscribe">
								<div class="row">
									<div class="col-md-12 col-md-offset-0">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Enter your email">
											<button type="submit" class="btn btn-primary">Subscribe</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->


    @stop


