@extends('layouts.dashboard')

<script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Admin</h3>
            </div>

            <div class="box-body">

            <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                            <div class="card-body">
                            <form action="{{ route('barang.update', $barang)}}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <div class="form-group row">
                                                <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>

                                                <div class="col-md-6">
                                                    <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{$barang->nama}}" required autofocus>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kategori" class="col-md-4 col-form-label text-md-right">{{ __('Kategori') }}</label>

                                                <div class="col-md-6">
                                                    <select name="kategori" class="form-control">
                                                        @foreach ($kategoris as $kategori)   <!-- $categories dari PostController -->
                                                        <option value="{{$kategori->kategori}}"
                                                        @if ($kategori->kategori === $barang->kategori)
                                                            selected
                                                        @endif
                                                        > {{$kategori->kategori}} </option>
                                                                @endforeach
                                                       </select>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="harga" class="col-md-4 col-form-label text-md-right">{{ __('Harga') }}</label>

                                                <div class="col-md-6">
                                                    <input id="harga" type="text" class="form-control{{ $errors->has('harga') ? ' is-invalid' : '' }}" name="harga" value="{{$barang->harga}}" required autofocus>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="stok" class="col-md-4 col-form-label text-md-right">{{ __('Stok') }}</label>

                                                <div class="col-md-6">
                                                    <input id="stok" type="text" class="form-control{{ $errors->has('stok') ? ' is-invalid' : '' }}" name="stok" value="{{$barang->stok}}" required autofocus>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="penjual" class="col-md-4 col-form-label text-md-right">{{ __('Penjual') }}</label>

                                                <div class="col-md-6">
                                                    <input id="penjual" type="text" class="form-control{{ $errors->has('penjual') ? ' is-invalid' : '' }}" name="penjual" value="{{$barang->penjual}}" readonly autofocus>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi') }}</label>

                                                <div class="col-md-10">
                                                    {{-- <input id="deskripsi" type="text" class="form-control{{ $errors->has('deskripsi') ? ' is-invalid' : '' }}" name="deskripsi" value="{{$barang->deskripsi}}" required autofocus> --}}
                                                    <textarea id="deskripsi" type="text" class="form-control" name="deskripsi" value="{{ old('deskripsi') }}" rows="5" cols="40">{{$barang->deskripsi}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="video" class="col-md-4 col-form-label text-md-right">{{ __('Video') }}</label>

                                                <div class="col-md-6">
                                                    <input id="video" type="text" class="form-control{{ $errors->has('video') ? ' is-invalid' : '' }}" name="video" value="{{$barang->video}}" required autofocus>

                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="promosi" class="col-md-4 col-form-label text-md-right">{{ __('Promosi') }}</label>

                                                <div class="col-md-6">
                                                    <input id="promosi" type="text" class="form-control{{ $errors->has('promosi') ? ' is-invalid' : '' }}" name="promosi" value="{{$barang->promosi}}" required autofocus>

                                                </div>
                                            </div> --}}
                                            <div class="form-group row">
                                                <label for="foto" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>
                                                <div class="col-md-6">

                                                    @foreach($images as $file)
                                                    <table style="width:100%">
                                                        <tr>
                                                      <td><img class="card-img-top" src="{{ url('product/'.$file->nama)}}" height="60" width="75"><br></td>
                                                      <td align="right"><a name="deleteImage" id="deleteImage" class="deleteImage btn btn-danger" data-id="{{ $file->id }}" href="#" >Hapus Foto</a></td>
                                                    </tr>
                                                </table>
                                                <br>
                                                @endforeach
                                                <div class="input-group control-group increment" >
                                                    <input type="file" name="filename[]" class="form-control">
                                                    <div class="input-group-btn">
                                                      <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                                                    </div>
                                                  </div>
                                                  <div class="clone hide">
                                                    <div class="control-group input-group" style="margin-top:10px">
                                                      <input type="file" name="filename[]" class="form-control">
                                                      <div class="input-group-btn">
                                                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                                      </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <input id="promosi" type="hidden" class="form-control{{ $errors->has('promosi') ? ' is-invalid' : '' }}" name="promosi" value="kosong">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
            </div>
            <!-- /.box-body -->
    </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="#">BBDSG</a>
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script>
$(".deleteImage").click(function(e) {
var id = $(this).data("id");
var token = $("meta[name='csrf-token']").attr("content");
console.log("it Work");
// alert(id);
            $.ajax({
                type: 'post',
                url: 'http://'+document.location.hostname+'/smk/public/barang/deletefile/'+id,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "_method": 'delete',
                    "id": id
                },
                success: function (id) {
                    // alert(id);
                    // alert('success');
                    location.reload();
                },
                error: function (id) {
                     alert(id);
                }
    });
});

</script>
<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
      });
      $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
      });
    });
</script>
<!-- CK Editor -->
<script src="{{asset('bower_components/ckeditor/ckeditor.js')}}"></script>
<script>
    $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('deskripsi')
      //bootstrap WYSIHTML5 - text editor
      $('.textarea').wysihtml5()
    })
  </script>
@endsection
